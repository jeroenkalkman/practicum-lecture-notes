{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python Basics \n",
    "\n",
    "In this course, you will work using a platform called \"Jupyter Notebooks\". Jupyter notebooks are a way to combine formatted text (like the text you are reading now), Python code (which you will use below), and the result of your code and calculations all in one place. Go through these notebooks and run the examples. Try to understand their functioning and, if necessary, add code (such as print statements or variable overviews) to make it clear for you. In addition, there are exercises and practice cells where you can program for yourself. Don't be afraid to start coding yourself, writing code and making mistakes is the best way to learn Python.\n",
    "\n",
    "In this lecture, you will learn the basic concepts of programming in Python. To get started, we will first explain the basic concepts of what python is and how it works. \n",
    "\n",
    "**Learning objectives for this notebook:**\n",
    "\n",
    "* Student is able to start the python interpreter and run code in a notebook\n",
    "* Student can stop and start notebook kernels\n",
    "* Student is able to create variables\n",
    "* Student can use `%whos` to list the variables stored in the memory of the python kernel\n",
    "* Student is able to determine the type of a variable \n",
    "* Student is able to convert between different variable types (float, int, etc)\n",
    "* Student is able to collect user input using the `input()` command\n",
    "* Student is able to print variable values using the `print()` command\n",
    "\n",
    "## What is Python? And what are Jupyter Notebooks? \n",
    "\n",
    "Python is an (<a href=https://en.wikipedia.org/wiki/Interpreted_language>interpreted</a>) computer programming language. Using python, you can ask your computer to do specific things, like perform a calculation, draw a graph, load data from a file, or interact with the user. \n",
    "\n",
    "Every time you run python, either by running it on the command line, or by running it in a Jupyter notebook like you are now, a python **\"kernel\"** is created. This kernel is a copy of the python program (\"interpreter\") that runs continuously on your computer (until you stop it).\n",
    "\n",
    "Jupyter notebooks are a way to interact with the python kernel. Notebooks are divided up into \"cells\", which can be either a text (<a href=https://en.wikipedia.org/wiki/Markdown>markdown</a> format) cell, like this one, or a code cell  (containing your code), like the cell below it. \n",
    "\n",
    "The selected cell is surrounded by a box. If you type \"enter\" in a text cell you can start editing the cell. If you push \"Run\" above, or type \"Shift-Enter\", the code will be \"run\". If it is a code cell, it will run a command (see below). If it is a markdown cell, it will \"compile\" the markdown text language into formatted (HTML) text. \n",
    "\n",
    "You can give commands to this kernel by typing commands using the python language into the code cells of the notebook. Here, you can find an example of a code cell that contains a simple python command `print`, which prints a text string to the command line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Hello world\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To send this command to the python kernel, there are several options. First, select the cell (so that it is either blue or green), and then:\n",
    "\n",
    "1. Click on the **Run** button above in the toolbar. This will execute the cell and move you to the next cell.\n",
    "2. Push **Shift-Enter**: this will do the same thing\n",
    "3. Push **Control-Enter**: this will run the cell but leave it selected (useful if you want to re-run a cell a bunch of times)\n",
    "\n",
    "When you run the cell, the code will be sent to the python kernel, which will translate your python command into a binary language your computer CPU understands, send it to the CPU, and read back the answer. If the code you run produces an \"output\", meaning that the kernel will send something back to you, then the output that your code produces will be displayed below the code cell in the \"output\" section of the code cell. This is a scheme of what this process looks like \"behind the scenes\": \n",
    "\n",
    "<img width=60% src=\"behind_the_scenes.png\"></img>\n",
    "\n",
    "After you have run the code cell, a number will appear beside your code cell. This number tell you in which order that piece of code was sent to the kernel. Because the kernel has a \"memory\", as you will see in the next section, this number can be useful so that you remember in which order the code cells in your notebook were executed. \n",
    "\n",
    "In the example above, the code cell contained only a single line of code, but if you want, you can include as many lines as you want in your code cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Hello\")\n",
    "print(\"world\")\n",
    "print(\"Goodbye\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the above, the text in the code cell are all python commands. In addition, if you start a line in a code cell with a `#`, python will ignore this line of text. This is use to add **comments** to your code. It is good programming practice to use comments to explain what the code is doing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This will print out a message\n",
    "print(\"This is a message\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.1** Print your own string to the command line. Can you use special characters as well?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Python kernel has a memory\n",
    "\n",
    "In addition to asking python to do things for you, like the \"Hello world\" example above, you can also have python remember things for you. To do this, you can use the following syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In python, the `=` symbol represents the **assignment operator**: it is an instruction to **assign** the value of `5` to the variable `a`. If variable `a` already exists, it will be over-written with the new value. If variable `a` does not yet exist, then python will create a new variable for you automatically.\n",
    "\n",
    "For you, the cell above will create a \"variable\" named `a` in memory of the python kernel that has the value of 5. We can check this by printing the value of a:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we can also add a message to this by combining this with a message with the `print()` statement by combining things with commas:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"The value of a is\",a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.2** Combine a string, a variable, and a numerical values in a single `print` statement using the `,` separator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.3** Change the value of `a` to 7 by executing the following cell, and then re-run the **above** cell containing the command `print(a)` (the one with output `5`). What value gets printed now in that cell?  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see in notebooks that the location of your code doesn’t matter, but the order in which you execute them does!!\n",
    "\n",
    "We can also use variables to set the values of other variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = 0\n",
    "print(b)\n",
    "b = a \n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, if you execute a lot of cells, or maybe even re-execute a cell after changing its contents, you might lose track of what variables are defined in the memory of your python kernel. For this, there is a convenient built-in \"magic\" command called `%whos` that can list for you all the variables that have been defined in your kernel, along with their values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a=5 \n",
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_(Some notes about `%whos`: `%whos` is not a \"native\" command of the python language, but instead a \"built-in\" command that has been added by the creators of Jupyter. Because of this, you cannot use it outside of Jupyter / iPython...)_\n",
    "\n",
    "If we define some new variables, they will also appear in the list of defined variables if you execute `%whos`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = 10\n",
    "d = 15.5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case the variable named is displayed, its value, but also its type. Type defines the format in which a variable is stored in memory. In this case `int` stands for integer and `float` stands for floating point number, which is the usual way in which real numbers are stored in a computer. We will learn more about Python variable types below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Starting and stopping the kernel\n",
    "\n",
    "When you open a notebook for the first time, a new kernel will be started for you, which will have nothing in your memory. \n",
    "\n",
    "Important to understand: if you close the tab of your browser that has the notebook, Jupyter **will not** shut down the kernel! It will leave the kernel running and when you re-open the notebook in your browser, all the variables you defined will still be in the memory. You can test this by closing this notebook right now, clicking on the link to open it in the \"tree\" file browser tab of Jupyter, and then re-running the cell above with the command `%whos`. \n",
    "\n",
    "How do I shutdown a kernel? And how do I know if a notebook on my computer already has a kernel running? \n",
    "\n",
    "* First, as you may have noticed, when you closed this notebook and went back to the \"tree\" file brower, the notebook icon had turned green. This is one way that Jupyter tells you that a notebook file has a running kernel.\n",
    "\n",
    "* Second: in the <a href=\".\"> \"tree\" view </a> of the Jupyter interface, there is a link at the top to a tab \"Running\" that will show you all the running kernels and allow you to stop them manually. \n",
    "\n",
    "Sometimes, you may want to restart the kernel of a notebook you are working on. You may want to do this to clear all the variables and run all your code again from a \"fresh start\" (which you should always do before submitting an assignment!). You may also need to do this if your kernel crashes (the \"status\" of your kernel can be seen in the icons at the right-hand side of the Jupyter menu bar at the top of the screen). \n",
    "\n",
    "For this, there is both a menubar \"Kernel\" at the top, along with two useful buttons in the toolbar: \n",
    "\n",
    "* \"Stop\": tells the kernel to abort trying to run the code it is working on, but does not erase its memory\n",
    "* \"Restart\": \"kill\" the kernel (erasing its memory), and start a new one attached to the notebook.\n",
    "\n",
    "To see this in action, you can execute the following cell, which will do nothing other than wait for 10 minutes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from time import sleep\n",
    "sleep(10*60)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will notice that while a cell is running, the text beside it shows `In [*]:`. The `*` indicates that the cell is being executed, and will change to a number when the cell is finished. You will also see that the small circle beside the `Python 3` text on the right side of the Jupyter menu bar at the top of the page will become solid. Unless you have a lot of patience, you should probably stop the kernel, using the \"Stop\" button:\n",
    "\n",
    "<img src=\"stop_button.png\"></img>\n",
    "\n",
    "or the menu item \"Kernel / Interrupt\".\n",
    "\n",
    "**Exercise 1.4** List the stored variables using the `%whos` command. Subsequently, restart the kernel. What variables are stored in the memory of the kernel before and after the restart? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python variable types\n",
    "\n",
    "As we saw above, in Python, variable have a property that is called their \"type\". When you use the assignment operator `=` to assign a value to a variable, python will automatically pick a variable type it thinks fits best, even changing the type of an existing variable if it thinks it is a good idea. \n",
    "\n",
    "You have, in fact, already seen information about the types of variables in the `%whos` command again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Variable   Type     Data/Info\n",
      "-----------------------------\n",
      "a          int      1\n",
      "b          int      2\n",
      "c          float    3.3\n"
     ]
    }
   ],
   "source": [
    "a = 1\n",
    "b = 2\n",
    "c = 3.3\n",
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the second column, you can see the **type** that python chose for the variables we created. `int` corresponds to integer numbers, `float` corresponds to floating-point numbers. You can see that for variable `c`, python had to choose a `float` type (because 15.5 is not an integer), but for `a` and `b`, it chose integer types. \n",
    "\n",
    "_(In general, Python tries to choose a variable type that makes calculations the fastest and uses as little memory as possible.)_\n",
    "\n",
    "If you assign a new value to a variable, it can change the variables type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = a/2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because 5/2 = 2.5, Python decided to change the type of variable `a` from `int` to `float` after the assignment operation `a = a/2`. \n",
    "\n",
    "When you are using floating point numbers, you can also use an \"exponential\" notation to specify very big or very small numbers: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = 1.5e-8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The notation `1.5e-8` is a notation used in python to indicate the number $1.5 \\times 10^{-8}$.\n",
    "\n",
    "A third type of mathematical variable type that you may use in physics is a complex number:  \n",
    "\n",
    "https://en.wikipedia.org/wiki/Complex_number\n",
    "\n",
    "In python, you can indicate a complex number by using `1j`, which is the python notation for the complex number $i$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d = 1+1j"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The notation `1j` is special, in particular because there is **no space** between the number `1` and the `j`. This is how Python knows that you are telling it to make a complex number (and not just referring to a variable named `j`...). The number in front of the `j` can be any floating point number: for example,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "0.5*d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to the mathematical variable types listed above, there are also other types of variables in Python. A common one you may encounter is the \"string\" variable type `str`, which is used for pieces of text. To tell Python you want to make a string, you enclose the text of your string in either single forward quotes `'` or double forward quotes `\"`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "e = \"This is a string\"\n",
    "f = 'This is also a string'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also make multiline strings using three single quotes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "multi = \\\n",
    "'''\n",
    "This string\n",
    "has \n",
    "multiple lines.\n",
    "'''\n",
    "print(multi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note here that I have used a backslash: this a way to split Python code across multiple lines. \n",
    "\n",
    "Although it's not obvious, Python can also do \"operations\" on strings, the `+` mathematical opeartors we saw above also works with strings. \n",
    "\n",
    "**Exercise 1.5** Discover what the `+` operator does to a string, i.e. print the output of the sum of two strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is one more useful variable type we will introduce here: the \"boolean\" type `bool`. Boolean variable can have two values: `True` and `False`. You type them in directly as `True` and `False` with no quotes (you will see them turn green). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use boolean types much more later when we look at program control flow, but a simple example is the `if` statement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "if True:\n",
    "    print(\"True is always true.\")\n",
    "\n",
    "if g:\n",
    "    print(\"g is true!\")\n",
    "    \n",
    "if not g:\n",
    "    print(\"g is not true!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can try changing the value of `g` above to `False` and see what happens if you run the above code cell again.\n",
    "\n",
    "Also, useful to know: numbers (both `int` and `float`) can also be used in True / False statements! Python will interpret any number that is not zero as `True` and any number that is zero as `False`. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.6** Discover which numbers can be used as `True` and `False` in Python by changing the value of `g` above and re-running the cells."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Converting variables between different types\n",
    "\n",
    "We can also convert a value from one type to another by using functions with the same name as the type that we want to convert them to. Some examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "float(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int(7.63)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that when converting an `float` to an `int`, python does not round off the value, but instead drops all the numbers off after the decimal point (it \"trucates\" it). If we want to convert to an integer and round it off, we can use the `round()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "round(7.63)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Round also converts the data type to an `int`. \n",
    "\n",
    "The idea of converting one variable type to an another using functions based on the variable type you want to convert to works for many variable types. All variable types in python \"provide\" such a function. \n",
    "\n",
    "Sometimes, you have will lose information in this process: for example, converting a `float` to an `int`, we lose all the numbers after the decimal point. In this example, Python makes a guess at what you probably want to do, and decides to round off the floating point number to the nearest integer. \n",
    "\n",
    "Sometimes, Python can't decide what to do, and so it triggers an error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "float(1+1j)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A very useful feature is that Python can convert numbers into strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "str(7.54)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is actually what happens when you use the `print()` commands with a numeric value.\n",
    "\n",
    "But also very useful is that as long as your string is easily convertable to a number, python can do this for you too!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "float('5.74')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int('774')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "complex('5+3j')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.7** Define a list of parameters with as many types as possible, i.e. all the examples you see above and maybe a few more. Use `%whos` to see how they look inside the computers' memory. Try to change their format and rerun the `%whos` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your parameters list\n",
    "a=\n",
    "b=\n",
    "\n",
    "# Parameter formats in the computer\n",
    "%whos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python can do math\n",
    "\n",
    "Python has a set of math functions that are directly built in to the language. You can use python as a calculator! "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "1+1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calculations also work with variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 5\n",
    "print(a+1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.8** Discover what the following Python operators do by performing some math with them: `*`, `-`, `/`, `**`, `//`, `%`. Print the value of the mathematical operation to the command line in susequent cells."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Try out *"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Try out -"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Try out /"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Try out **"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Try out //"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Try out %"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another handy built-in function is `abs()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(abs(10))\n",
    "print(abs(-10))\n",
    "print(abs(1j))\n",
    "print(abs(1+1j))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can find the full list of built-in math commands on the python documentation webpage:\n",
    "\n",
    "https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-complex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sending data to python using input()\n",
    "\n",
    "So far, we have seen examples of \"output\": Python telling us stuff, like in the first \"Hello world\" example above. \n",
    "\n",
    "And we have seen example of \"code\": us giving instructions to python to do things. \n",
    "\n",
    "In addition, we can also send stuff to Python. Often in physics, we do this by having Python read data files, which we will cover later, but we can also send information to python using the `input()` command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = input()\n",
    "\n",
    "print()\n",
    "print(\"The input was:\")\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even if we type a number into the input box, it will always return a string variable of type `str`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want to use our input as a number, we have to convert it to a number, for example, by using the `float()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = input()\n",
    "a = float(a)\n",
    "print(\"\\nThe value of a is:\", a)\n",
    "print(\"a has the type:\", type(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also specify text for the label of the input box: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = input(\"Enter a number: \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.9** Use the `input` function to create variables in the kernel of the following types: an `integer`, a `float`, and a `string`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tab completion in Jupyter Notebooks\n",
    "\n",
    "\n",
    "Computer programmers often forget things. They even sometimes forget the names of the variables they have created themselves! Fruthermore, computer programmers always like to save typing if they can. \n",
    "\n",
    "For this reason, the people who made Jupyter notebooks included a handy feature called \"Tab completion\" (which is actually something that has been around for <a href=https://en.wikipedia.org/wiki/Command-line_completion>a long time</a> in unix command line environments). \n",
    "\n",
    "The idea is that if you start typing part of the name of a variable or part of the name of a function, and then push the `Tab` key, Jupyter will bring up a list of the variable and function names that match what you have started to type. If ony one matches, it will automatically type the rest for you. If multiple things match, it will offer you a list: you can either keep typing until it's unique and type tab again, or you can use the cursor keys to select the one you want.\n",
    " \n",
    "Here is an example: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "this_is_my_very_long_variable_name = 5\n",
    "this_is_another_ones = 6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now click on the following code cell, go the end of the line in this cell and try pushing `Tab`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "this"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Handy! Jupyter did the typing for me! \n",
    "\n",
    "If multiple things match, you will get a drop-down box and can select the one you want. If you want, you can then use the arrow keys to pick one of them and type `Enter` to finish the tab completion. Try it here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "th"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also keep on typing: if you just type `a` after you hit tab and then hit tab again, it will finish the typing for you. Try it here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "th"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.10** Use tab completion on the initial letters of a few of the commands that have been presented. Along the way you will discover many more Python commands!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Understanding Python Errors\n",
    "\n",
    "Sometimes, the code you type into a code cell will not work. In this case, Python will not execute your code, but instead print out an error message. In this section, we will take a look at these error messages and learn how to understand them.\n",
    "\n",
    "Let's write some code that will give an error. For example, this is a typo in the name of the `print()` command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "a = 5\n",
    "printt(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After your code cell, you will see some colored text called a \"Traceback\". This \"Traceback\" is the way that python tries to tell you where the error is. \n",
    "\n",
    "Let's take a look at the traceback:\n",
    "\n",
    "<img src=\"anatomy_of_an_error.png\" width=80%></img>\n",
    "\n",
    "The traceback contains three important details that can help you:\n",
    "\n",
    "1. The type of error\n",
    "2. Where the error occurred in your code\n",
    "3. An attempt to explain why the error happened\n",
    "\n",
    "For 1 and 2, Python is pretty good and will communicate clearly. For 3, sometimes you need to have some experience to understand what python is trying to tell you.\n",
    "\n",
    "In this specific case, the type was a `NameError` that occured on line 2 of our code cell. \n",
    "*(By the way, in the View menu, you can turn on and off line numbers in your cells.)* \n",
    "\n",
    "A `NameError` means that python tried to find a function or variable that you have used, but failed to find one. If you look already at the line of code, you can probably spot the problem already.\n",
    "\n",
    "At the very end of the traceback, Python tries to explain what the problem was: in this case, it is telling you that there is no function named `printt`. \n",
    "\n",
    "You will also get a `NameError` if you try to use a variable that doesn't exist:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(non_existant_variable)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another common type of error is a `SyntaxError`, which means you have typed something that python does not understand:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "a = a $ 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also get errors if you try to use operators that do not work with the data type you have. For example, if you try to \"divide\" two strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"You cannot \" / \"divide strings\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, you get a `TypeError`: the division operator is a perfectly fine syntax, it just does not work with strings. \n",
    "\n",
    "\n",
    "In python, errors are also called \"Exceptions\", and a complete list of all error (exception) types, and what they mean, can be found here:\n",
    "\n",
    "https://docs.python.org/3/library/exceptions.html#concrete-exceptions\n",
    "\n",
    "Sometimes, you can learn more about what the error means by reading these documents, although they are perhaps a bit hard to understand for beginners. \n",
    "\n",
    "In last resort, you can also always try a internet search: googling the error message can help, and there are also lots of useful posts on <a href=https://stackexchange.com>stack exchange</a> (which you will also often find by google)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1.11** Run the following code and try to understand what is going wrong by reading the error message."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "a=10\n",
    "b=0\n",
    "c=(a/b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "4 + practicum*3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d='practicum is great' + 2"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "402.797px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
