A repository for storing the material for the programming portion of the 
1st-year Applied Physics practicum  course. 

* Lecture notebooks
* Course information notebooks

To get meaningful diff informatiton, we will store the notebooks paired with 
markdown files using Jupytext.

The assignments and final project files are stored in a separate (private) 
repository.